import numpy as np
import scipy.optimize as spo
import matplotlib.pyplot as plt


class Minimizer:
    """
     Steps on how to use an optimizer:

     1. Define function to minimize
     2. Provide an initial guess
     3. Call the Optimizer

    """
    def __init__(self):
        print('Initial Minimizer Example Using SciPy')

    def _f(self, X):
        """Given a scalar X, return some value as a real number."""
        Y = (X - 1.5)**2 + 0.5
        print('X = {}, Y = {}'.format(X, Y))
        return Y

    def test_run(self, Xguess = 2.0):
        min = spo.minimize(self._f, Xguess, method='SLSQP', options={'disp': True})
        print('Minimum Values are X: {}, Y: {}'.format(min.x, min.fun))
        return min


    def plot(self, x, y):
        Xplot = np.linspace(0.5, 2.5, 22)
        Yplot = self._f(Xplot)
        plt.plot(Xplot, Yplot)
        plt.plot(x, y, 'ro')
        plt.title('Minima of parabola function')
        plt.show()