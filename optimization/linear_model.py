import numpy as np
import scipy.optimize as spo
import matplotlib.pyplot as plt


class LinearModel:

    def error_function(self, line, data):
        """Compute error between given line model and observed data,
        in an attempt to fit the data.

        :param line: tuple of coefficients (C0, C1) of linear y = mx + b,  where C0 is slope, C1 is y-int
        :param data: 2D array where each row is a point (x, y)
        """
        # print(data[:-1])
        print('data {}'.format(data))

        print('Every X: each row in column 0 data[:,0] {}'.format(data[:, 0]))
        print('Every Y: each row in column 1 data[:,1] {}'.format(data[:, 1]))

        ysum = np.sum(data[:, 1])  # sum every row of column 1
        print(ysum)

        order_ops = data[:, 0] + data[:, 1]  # [(1 + 1.5), (2 + 2.5)]
        print(order_ops)
        err = np.sum((data[:, 1] - (line[0] * data[:, 0] + line[1])) ** 2)
        print(err)
        return err

    def test_run(self, coefficient_guess=(1, 0), data=np.asarray([])):
        min = spo.minimize(self.error_function,
                           coefficient_guess,
                           data,
                           method='SLSQP',
                           options={'disp': True})
        print('Minimum error evaluated: {}'.format(min))
        return min

    def plot_lines(self, lines):
        for line in lines:
            color = line['color']
            label = line['label']
            line_coefficients = line['line']
            x = np.linspace(0, 10, 21)
            y = line_coefficients[0] * x + line_coefficients[1]
            print('Line C0 = {} and C1 = {}'.format(line['line'][0], line['line'][1]))
            plt.plot(x, y, color, linewidth=2.0, label=label)
        plt.legend(loc='upper left')
        plt.show()
