# Machine Learning Playground for Expanse LLC Finance Project

### Authors: 
Meghan Erickson

### Summary
This is where I POC ideas and formulas before implementing them against the real portfolio data.

+ Optimization

### Technologies
+ Python 3
+ Pandas, Numpy
+ Scipy

### Getting Started


```bash
python3 -m venv .venv/

source .venv/bin/activate

pip3 install -r requirements.txt

python3 main.py
```

### Helpful Notes

+ Slicing
```python

a[start:end] # items start through end-1
a[start:]    # items start through the rest of the array
a[:end]      # items from the beginning through end-1
a[:]         # a copy of the whole array

a[start:end:step] # start through not past end, by step

a[-1]    # last item in the array
a[-2:]   # last two items in the array
a[:-2]   # everything except the last two items

a[::-1]    # all items in the array, reversed
a[1::-1]   # the first two items, reversed
a[:-3:-1]  # the last two items, reversed
a[-3::-1]  # everything except the last two items, reversed
```

### Minimizer Using SciPy
- See minimizer.py for a basic example in code
- General Steps on How to Use an Optimizer
  - Define a Function to Minimize
  - Provide an Initial Guess
  - Call the Optimizer `scipy.optimize.minimize`
- Minimizers work with [Convex Functions](https://en.wikipedia.org/wiki/Convex_function)

