from optimization.linear_model import LinearModel
from optimization.minimizer import Minimizer
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    print('Welcome to Expanse LLC Machine Learning Playground')

    print('Basic Minimizer Example')
    minimizer = Minimizer()


    # min = minimizer.test_run()
    # minimizer.plot(min.x, min.fun)

    print('Experiment 1: Fit a Line to Given Data Points')
    '''
        data = 2d array where each element is a point (x,y)
    '''
    lm = LinearModel()
    line_coefficients = np.float32([2, 3])
    data = np.asarray([(1, 2), (1.5, 2.5)]).T
    err = lm.error_function(line_coefficients, data)
    min = lm.test_run(line_coefficients, data)


    lines = []
    line1 = {
        'label': 'Original Guess Line',
        'line': line_coefficients,
        'color': 'b--'
        }
    line2 = {
        'label': 'Fitted Line',
        'line': min.x,
        'color': 'r--'
    }
    lines.append(line1)
    lines.append(line2)

    lm.plot_lines(lines)
